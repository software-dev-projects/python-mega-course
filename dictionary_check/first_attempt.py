import spacy
import nltk
import cloudpickle
nltk.download("words")
#from nltk.corpus import words
dictionary = open("3000_most_common.txt",'rb').readlines()
dictionary=list(map(lambda x:x.strip(),dictionary))
dictionary=list(map(lambda x:x.decode("utf-8"),dictionary))
data = cloudpickle.load(open("cleaned_text.p","rb"))

nlp = spacy.load('en')
not_present = []
not_present_id = []


for row in data.iterrows():
    for token in nlp(row[1][1]):
        if token.text not in dictionary:
            if token.text not in not_present:
                not_present_id.append(row[1][0])
                not_present.append(token.text)

paired_list = dict(zip(not_present_id,not_present))
with open("not_present.p", 'wb') as pickle_file:
    cloudpickle.dump(paired_list, pickle_file)

print(1)