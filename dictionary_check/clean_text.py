import textacy
import pandas as pd
import cloudpickle

data = pd.read_csv('test_set_10000.csv')
data['PresentingComplaint'] = data['PresentingComplaint'].str.lower()
unprocessed_complaints = data['PresentingComplaint']
processed_complaints = pd.Series([])
for index, row in enumerate(unprocessed_complaints):
    processed_complaints[index] = textacy.preprocess_text(row,no_punct=True,no_contractions=True)

data['PresentingComplaint'] = processed_complaints

cloudpickle.dump(data,open("cleaned_text.p","wb"))