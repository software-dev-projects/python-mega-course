import folium
map1 = folium.Map(location=[38.58,99.09],zoom_start = 6  )

fg = folium.FeatureGroup(name = "My Map")
fg.add_child(folium.Marker(location=[38.55,99.00], popup="Me marker", icon=folium.Icon(color='green')))

map1.add_child(fg)
map1.save("Map1.html")

