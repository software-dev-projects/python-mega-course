import pandas as pd
import folium

def get_colour(elevation):
    if elevation<1500:
        return 'green'
    elif 1500<elevation<3000:
        return 'orange'
    else:
        return 'red'

df = pd.read_csv("Volcanoes_USA.txt")
coordinates = df[['ELEV','NAME','LAT','LON']]
map1 = folium.Map(location=[39.0501945,-114.692732],zoom_start = 5  )

fg = folium.FeatureGroup(name = "volcanoes")

for row in coordinates.iterrows():
    print(row)
    fg.add_child(folium.CircleMarker(location=[row[1]['LAT'], row[1]['LON']],
                               popup=folium.Popup(str(row[1]['NAME']),parse_html=True),
                               fill_color=get_colour(row[1]['ELEV']),radius=6,fill_opacity=0.7, color="grey"))

fg.add_child(folium.GeoJson(data=(open('world.json','r',encoding='utf-8-sig'
                                  ).read()),style_function=lambda x: {'fillColor':'yellow' if x['properties']['POP2005']<100 else
'orange' if 10000<=x['properties']['POP2005']<20000000 else 'red'}))
map1.add_child(fg)
map1.add_child(folium.LayerControl())
map1.save("Map1.html")

df.join
print(1)