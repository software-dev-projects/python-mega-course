from datetime import datetime as dt
import time as t

def run_website_blocker(work_start_time = 8, work_end_time=16, working_weekends = False):
    hosts_temp = "hosts"
    hosts_path="C:\Windows\System32\drivers\etc\hosts"
    redirect = "127.0.0.1"
    website_list = ['www.facebook.com', "facebook.com"]

    while True:
        if working_weekends or dt.now().weekday()<=4:
            if work_start_time<dt.now().hour<work_end_time:
                print("Working hours ...")
                with open(hosts_path,'r+') as file:
                    content = file.read()
                    for website in website_list:
                        if website not in content:
                            file.write(redirect+"    "+website+"\n")
            else:
                with open(hosts_path,'r+') as file:
                    content = file.readlines()
                    file.seek(0)
                    for line in content:
                        if not any(website in line for website in website_list):
                            file.write(line)
                    file.truncate()
                print("Fun time ;D")
        t.sleep(5)

run_website_blocker()